package it.ggwp.hashcode;

import it.ggwp.hashcode.logic.SlideMaker;
import it.ggwp.hashcode.logic.TagCompare;
import it.ggwp.hashcode.model.Slide;
import it.ggwp.hashcode.util.Reader;
import it.ggwp.hashcode.util.Writer;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        int size = Reader.getSize();

        Reader.getInputFile();
        SlideMaker.fillSlideshowHorizontal();
        SlideMaker.fillSlideshowVertical();

        ArrayList<Slide> ord = TagCompare.createOrderedSlideshow();
        Writer.write(ord);
    }
}
