package it.ggwp.hashcode.logic;

import it.ggwp.hashcode.model.Catalog;
import it.ggwp.hashcode.model.Photo;
import it.ggwp.hashcode.model.Slide;
import it.ggwp.hashcode.model.Slideshow;

import java.util.ArrayList;

public class SlideMaker {

    public static void fillSlideshowHorizontal() {
        ArrayList<Photo> allPhotos = Catalog.getInstance().getCatalog();

        for (int i = 0; i < Catalog.getInstance().getCatalogSize(); i++) {
            if (allPhotos.get(i).isHorizontal()) {
                Slide slide = new Slide(Slideshow.index, allPhotos.get(i));
                Slideshow.getInstance().getSlideshow().add(slide);
            }
        }
    }

    public static void fillSlideshowVertical() {
        ArrayList<Photo> allPhotos = Catalog.getInstance().getCatalog();

        for (int i = 0; i < Catalog.getInstance().getCatalogSize(); i++) {
            if (!allPhotos.get(i).isHorizontal()) {
                int next = getNextVerticalIndex(i);

                if (next != -1) {
                    Slide slide = new Slide(Slideshow.index, allPhotos.get(i), allPhotos.get(next));
                    Slideshow.getInstance().getSlideshow().add(slide);
                    i = next;
                } else {
                    Slide slide = new Slide(Slideshow.index, allPhotos.get(i));
                    Slideshow.getInstance().getSlideshow().add(slide);
                    return;
                }
            }
        }
    }

    private static int getNextVerticalIndex(int start) {
        ArrayList<Photo> allPhotos = Catalog.getInstance().getCatalog();

        for (int i = start + 1; i < Catalog.getInstance().getCatalogSize(); i++) {
            if (!allPhotos.get(i).isHorizontal()) {
                return i;
            }
        }

        return -1;
    }
}
