package it.ggwp.hashcode.logic;

import it.ggwp.hashcode.model.Slide;
import it.ggwp.hashcode.model.Slideshow;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class TagCompare {

    public static int getCommonTags(ArrayList<String> tagA, ArrayList<String> tagB) {
        int common = 0;

        for (String s : tagA) {
            for (String s2 : tagB) {
                if (s.equals(s2))
                    common++;
            }
        }

        return common;
    }

    public static int getScoreSlides(Slide s1, Slide s2) {
        ArrayList<String> tag1 = new ArrayList<>();
        ArrayList<String> tag2 = new ArrayList<>();

        if (s1.isDoppia()) {
            tag1.addAll(s1.getContent().get(0).getTags());
            tag1.addAll(s1.getContent().get(1).getTags());
        } else {
            tag1.addAll(s1.getContent().get(0).getTags());
        }

        if (s2.isDoppia()) {
            tag2.addAll(s2.getContent().get(0).getTags());
            tag2.addAll(s2.getContent().get(1).getTags());
        } else {
            tag2.addAll(s2.getContent().get(0).getTags());
        }

        int common = getCommonTags(tag1, tag2);
        int left1 = tag1.size() - common;
        int left2 = tag2.size() - common;

        return Math.min(Math.min(common, left1), left2);
    }

    public static ArrayList<Slide> createOrderedSlideshow() {
        ArrayList<Slide> ordered = new ArrayList<>();
        ArrayList<Slide> all = Slideshow.getInstance().getSlideshow();
        ArrayList<Integer> index = new ArrayList<>();

        for (int i = 0; i < all.size(); i++) {
            int randomNum;
            do {
                randomNum = ThreadLocalRandom.current().nextInt(0, all.size());
            } while (index.contains(randomNum));

            ordered.add(all.get(randomNum));
            index.add(randomNum);
        }

        return ordered;
    }
}
