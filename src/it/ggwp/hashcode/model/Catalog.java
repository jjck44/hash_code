package it.ggwp.hashcode.model;

import java.util.ArrayList;

public class Catalog {
    private static Catalog instance;

    public static Catalog getInstance() {
        if (instance == null) {
            instance = new Catalog();
        }

        return instance;
    }

    private ArrayList<Photo> catalog;

    public Catalog() {
        catalog = new ArrayList<>();
    }

    public ArrayList<Photo> getCatalog() {
        return catalog;
    }

    public int getCatalogSize() {
        return catalog.size();
    }

    public ArrayList<Photo> getAllPhotos() {
        return catalog;
    }
}
