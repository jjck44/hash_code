package it.ggwp.hashcode.model;

import java.util.ArrayList;

public class Photo {
    //ID foto
    private long id;

    //Se vero, orizzontale. Se falso, verticale
    private boolean horizontal;

    //Numero di tag
    private int tagNum;

    //Lista dei tag
    private ArrayList<String> tags;

    public Photo(long id, boolean horizontal, int tagNum, ArrayList<String> photoTags) {
        tags = new ArrayList<>();

        this.id = id;
        this.horizontal = horizontal;
        this.tagNum = tagNum;
        tags.addAll(photoTags);
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public int getTagNum() {
        return tagNum;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public long getId() {
        return id;
    }
}
