package it.ggwp.hashcode.model;

import java.util.ArrayList;

public class Slide {
    private int id;

    //Se è doppia verticale
    private boolean doppia;

    //Contenuto slide, 1 o 2 foto
    private ArrayList<Photo> content;

    public Slide(int id, Photo photo) {
        Slideshow.index++;
        content = new ArrayList<>();

        this.id = id;
        this.doppia = false;
        content.add(photo);
    }

    public Slide(int id, Photo photo1, Photo photo2) {
        Slideshow.index++;
        content = new ArrayList<>();

        this.id = id;
        this.doppia = true;
        content.add(photo1);
        content.add(photo2);
    }

    public int getId() {
        return id;
    }

    public boolean isDoppia() {
        return doppia;
    }

    public ArrayList<Photo> getContent() {
        return content;
    }
}
