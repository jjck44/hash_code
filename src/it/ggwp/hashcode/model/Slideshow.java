package it.ggwp.hashcode.model;

import java.util.ArrayList;

public class Slideshow {
    public static int index = 0;
    private static Slideshow instance;

    public static Slideshow getInstance() {
        if (instance == null) {
            instance = new Slideshow();
        }

        return instance;
    }

    private ArrayList<Slide> slideshow;

    public Slideshow() {
        slideshow = new ArrayList<>();
    }

    public ArrayList<Slide> getSlideshow() {
        return slideshow;
    }

    public int getSlideshowLength() {
        return slideshow.size();
    }
}
