package it.ggwp.hashcode.util;

import it.ggwp.hashcode.model.Catalog;
import it.ggwp.hashcode.model.Photo;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reader {

    public static void getInputFile() throws IOException {
        List<String> lines = FileUtils.readLines(new File("src\\input\\c_memorable_moments.txt"), "UTF-8");

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 2; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 3; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 4; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 5; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 6; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 7; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 8; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 9; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 10; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 11; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 12; i < getSize(); i += 14) {

                    String[] line = lines.get(i + 1).split(" ");

                    boolean mode = true;

                    if (line[0].equalsIgnoreCase("V")) {
                        mode = false;
                    }

                    ArrayList<String> tags = new ArrayList<>();

                    /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                        tags.add(line[2 + j]);

                    }*/

                    Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

                    Catalog.getInstance().getCatalog().add(newPhoto);
                }
            }
        }).start();


        for (int i = 13; i < getSize(); i += 14) {

            String[] line = lines.get(i + 1).split(" ");

            boolean mode = true;

            if (line[0].equalsIgnoreCase("V")) {
                mode = false;
            }

            ArrayList<String> tags = new ArrayList<>();

            /*for (int j = 0; j < Integer.parseInt(line[1]); j++) {

                tags.add(line[2 + j]);

            }*/

            Photo newPhoto = new Photo(i, mode, Integer.parseInt(line[1]), tags);

            Catalog.getInstance().getCatalog().add(newPhoto);
        }
    }

    public static int getSize() {

        try {
            List<String> lines = FileUtils.readLines(new File("src\\input\\c_memorable_moments.txt"), "UTF-8");

            return Integer.parseInt(lines.get(0));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

}
