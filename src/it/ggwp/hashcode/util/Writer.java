package it.ggwp.hashcode.util;

import it.ggwp.hashcode.model.Slide;
import it.ggwp.hashcode.model.Slideshow;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Writer {
    public static void write(ArrayList<Slide> ord) throws IOException {

        ArrayList<String> daScrivere = new ArrayList<>();

        daScrivere.add("" + ord.size());

        for (int i = 0; i < ord.size(); i++) {
            if (ord.get(i).isDoppia()) {
                daScrivere.add("" + ord.get(i).getContent().get(0).getId() +
                        " " + ord.get(i).getContent().get(1).getId());
            } else {
                daScrivere.add("" + ord.get(i).getContent().get(0).getId());
            }
        }

        FileUtils.writeLines(new File("src\\output\\a_output.txt"), daScrivere);
    }
    public static void write() throws IOException {

        ArrayList<String> daScrivere = new ArrayList<>();

        daScrivere.add("" + Slideshow.getInstance().getSlideshowLength());

        for (int i = 0; i < Slideshow.getInstance().getSlideshowLength(); i++) {
            if (Slideshow.getInstance().getSlideshow().get(i).isDoppia()) {
                daScrivere.add("" + Slideshow.getInstance().getSlideshow().get(i).getContent().get(0).getId() +
                        " " + Slideshow.getInstance().getSlideshow().get(i).getContent().get(1).getId());
            } else {
                daScrivere.add("" + Slideshow.getInstance().getSlideshow().get(i).getContent().get(0).getId());
            }
        }

        FileUtils.writeLines(new File("src\\output\\a_output.txt"), daScrivere);
    }
}
